package ua.org.oa.honcharov.HomeWork_1;

public class Date {

    private Day day;
    private Month month;
    private Year year;
    private boolean leapYear;

    public Date(int day, int month, int year) {
        this.day = new Day(day);
        this.month = new Month(month);
        this.year = new Year(year);
    }

    public Day getDay() {
        return day;
    }

    public Month getMonth() {
        return month;
    }

    public Year getYear() {
        return year;
    }

    public boolean isLeapYear() {
        return leapYear;
    }

    public void setLeapYear(boolean leapYear) {
        this.leapYear = leapYear;
    }

    public class Day {

        private int day;

        public Day(int day) {
            setDay(day);
        }

        public void setDay(int day) {
            this.day = day;
        }


        public int getDay() {
            return day;
        }

        @Override
        public String toString() {
            return "day = " + getDay();
        }
    }

    public class Month {

        private int month;

        public Month(int month) {
            setMonth(month);
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getDays(int month, boolean leapYear) {
            int i = 0;
            switch (month) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    i = 31;
                    break;
                case 2:
                    if (leapYear) {
                        i = 29;
                        break;
                    }
                    i = 28;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    i = 30;
                    break;
            }
            return i;
        }

        @Override
        public String toString() {
            return "month = " + getMonth();
        }
    }

    public class Year {

        private int year;

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public Year(int year) {
            setYear(year);
            setLeapYear(java.time.Year.of(year).isLeap());
        }

        @Override
        public String toString() {
            return "year = " + getYear();
        }
    }

    public enum DayOfWeek {
        MONDAY(0),
        TUESDAY(1),
        WEDNESDAY(2),
        THURSDAY(3),
        FRIDAY(4),
        SATURDAY(5),
        SUNDAY(6);

        public final int number;

        private DayOfWeek(int number) {
            this.number = number;
        }
    }

    public DayOfWeek getDayOfWeek() {
        DayOfWeek dow = DayOfWeek.MONDAY;
        ;
        switch (calkDayForFirstWeek() - 1) {
            case 0:
                dow = DayOfWeek.MONDAY;
                break;
            case 1:
                dow = DayOfWeek.TUESDAY;
                break;
            case 2:
                dow = DayOfWeek.WEDNESDAY;
                break;
            case 3:
                dow = DayOfWeek.THURSDAY;
                break;
            case 4:
                dow = DayOfWeek.FRIDAY;
                break;
            case 5:
                dow = DayOfWeek.SATURDAY;
                break;
            case 6:
                dow = DayOfWeek.SUNDAY;
                break;
        }
        return dow;
    }

    public int getDayOfYear() {
        int calkDay = 0;
        for (int i = 0; i < month.getMonth(); i++) {
            calkDay += month.getDays(i, isLeapYear());
        }
        return calkDay += day.getDay();
    }

    public int daysBetween(Date date) {
        return 0;
    }

    public int calkDayForFirstWeek() {          // Метод для приведения заданого дня к дням первой недели
        // (Пример: если наш день 23 то он будет таким же днем недели как и 2. При условии, что у всех месяцов 1 число это понедельник.)
        int i = 0;

        if (day.getDay() <= 7) {
            i = day.getDay();
        } else if (day.getDay() <= 14) {        // тут мы должны отнять от "day.getDay()" 7
            i = (day.getDay() - 7);
        } else if (day.getDay() <= 21) {        // тут мы должны отнять от "day.getDay()" 14
            i = (day.getDay() - 14);
        } else if (day.getDay() <= 28) {        // тут мы должны отнять от "day.getDay()" 21
            i = (day.getDay() - 21);
        } else if (day.getDay() <= 31) {        // тут мы должны отнять от "day.getDay()" 28
            i = (day.getDay() - 28);
        }
        return i;
    }


    @Override
    public String toString() {
        return "Date: " + getDay() + ", " + getMonth() + ", " + getYear() + ".";
    }
}
