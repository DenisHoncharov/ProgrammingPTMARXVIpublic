package ua.org.oa.honcharov.HomeWork_1;

/**
 * Created by den4ik on 14.04.2016.
 */
public class Car {
    private String company;
    private String model;
    private int price;

    public Car(String company, String model, int price) {
        setCompany(company);
        setModel(model);
        setPrice(price);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" + "company='" + getCompany() + ", model='" + getModel() + ", price=" + getPrice() + '}';
    }
}
