package ua.org.oa.honcharov.HomeWork_1;

/**
 * Created by den4ik on 09.04.2016.
 */
public class App {

    public static void main(String[] args) {
        System.out.println();
        System.out.println("ex. 1");

        Date date = new Date(28, 3, 2007);
        System.out.println(date.daysBetween(new Date(20, 3, 2007)));

        System.out.println();
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println();
        System.out.println("ex. 2");

        Car car = new Car("BMW", "e30", 50000) {
            @Override
            public String toString() {
                return "Car { " + "Company = " + getCompany() + ", price = " + getPrice() + '}';
            }
        };

        System.out.println(car);

        Car car1 = new Car("Mercedes", "c63", 150000) {
            @Override
            public String toString() {
                return "Car{" + " Model = " + getModel() + ", price = " + getPrice() + '}';
            }
        };

        System.out.println(car1);

    }
}
