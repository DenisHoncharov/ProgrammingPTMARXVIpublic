package ua.org.oa.honcharov.HomeWork_3;

/**
 * Created by den4ik on 15.05.2016.
 */
public class App {
    public static void main(String[] args) {
        GenericStorage<String> gs1 = new GenericStorage<>();
        gs1.add("mama");
        gs1.add("mila");
        gs1.add("ramy");
        gs1.add("papa");
        gs1.add("mil");
        gs1.add("mashiny");

        System.out.println(gs1.get(5));
        System.out.println("~~~~~~~~~~~~~~~");
        System.out.println(gs1.getAll());
        System.out.println("~~~~~~~~~~~~~~~");
        gs1.update(5, "i ne pomil");
        gs1.delete(6);
        gs1.delete("mama");
        System.out.println(gs1.tArrayCapacity());
    }
}
