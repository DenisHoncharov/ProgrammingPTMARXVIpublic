package ua.org.oa.honcharov.HomeWork_3;

import java.util.Arrays;

/**
 * Created by den4ik on 15.05.2016.
 */
public class GenericStorage<T> {

    private T[] tArray = null;
    private int indexLastElement = 0;

    public GenericStorage() {
        settArray(10);
    }

    public GenericStorage(int size) {
        settArray(size);
    }

    public void settArray(int size) {
        this.tArray = (T[]) (new Object[size]);  //задает размер массива
    }

    public boolean add(T obj) {
        if (tArray.length == -1 && tArray.length != indexLastElement) {
            tArray[0] = obj;
            this.indexLastElement++;
            return true;
        } else if (tArray.length != -1 && tArray.length != indexLastElement) {
            tArray[indexLastElement] = obj;
            indexLastElement++;
            return true;
        }
        return false;
    }

    public T get(int index) {
        for (int i = 0; i < tArray.length; i++) {
            if (i == index) {
                return tArray[i];
            }
        }
        return null;
    }

    public String getAll() {
        return Arrays.toString(tArray);
    }

    public boolean update(int index, T obj) {
        if (index < tArray.length && index > -1) {
            tArray[index] = obj;
            return true;
        }
        return false;
    }

    public boolean delete(int index) {
        for (int i = 0; i < tArray.length; i++) {
            if (i == index && i == tArray.length - 1) {
                tArray[index] = null;
                return true;
            } else if (i == index && i != tArray.length - 1) {
                for (int j = i; j < tArray.length-1; j++) {
                    tArray[j] = tArray[j + 1];
                    tArray[indexLastElement] = null;
                }
                indexLastElement--;
                return true;
            }
        }
        return false;

    }

    public boolean delete(T obj) {
        int i = 0;
        for (T t : tArray) {
            if (t.equals(obj)) {
                tArray[i] = null;
                return true;
            }
            i++;
        }
        return false;
    }

    public int tArrayCapacity() {
        return indexLastElement;
    }
}
