package ua.org.oa.honcharov.PZ_1;

/**
 * Created by student on 04.04.2016.
 */
public class ArrayProd {
    public static int multiplyArray(int[] multiArray){
        int multiply = 1;
        for (int i : multiArray) {
            multiply *= i;
        }
        return multiply;
    }
}
