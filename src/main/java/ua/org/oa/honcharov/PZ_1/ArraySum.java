package ua.org.oa.honcharov.PZ_1;

/**
 * Created by student on 04.04.2016.
 */
public class ArraySum {

    private int[] myArray;

    public int[] getMyArray() {
        return myArray;
    }

    public void setMyArray(int[] myArray) {
        this.myArray = myArray;
    }


    public ArraySum (int[] myArray){
       setMyArray(myArray);
    }

    public int sum (){
        int sum = 0;
        for (int i : myArray){
            sum += myArray[i];
        }
        return sum;
    }

    public static int sum(int[] array){
        int sum = 0;
        for (int i = 0; i < array.length; i++){
            sum += array[i];
        }
        return sum;
    }
}
