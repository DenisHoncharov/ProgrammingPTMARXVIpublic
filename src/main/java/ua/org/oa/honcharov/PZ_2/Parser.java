package ua.org.oa.honcharov.PZ_2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 Метод парс. Будет принимать адресс файла и будет его вычитывать.
 Метод для читания с файла.
 */
public class Parser {

    List<Bin> names = new ArrayList();


    String my;

    public String reader () throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("baby2008.html"));
        StringBuilder sb = new StringBuilder();
        String s = null;
        while ((s = br.readLine()) != null){
            sb.append(s);
        }
        return sb.toString();
    }

    public void parcer () throws IOException {
        String textReadyToParce = reader();

        String regex = "(<.*?>)(\\d+)(<.*?><.*?>)(\\w+)(<.*?><.*?>)(\\w+)(<.*?>)";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(textReadyToParce);
        while(matcher.find()) {
            names.add(new Bin(matcher.group(2), matcher.group(4), matcher.group(6)));
        }
    }

}
