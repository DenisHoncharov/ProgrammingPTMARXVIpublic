package ua.org.oa.honcharov.PZ_2;

/**
 * Created by student on 11.04.2016.
 */
public class Bin {

    private String number;
    private String manName;
    private String womanName;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getManName() {
        return manName;
    }

    public void setManName(String manName) {
        this.manName = manName;
    }

    public String getWomanName() {
        return womanName;
    }

    public void setWomanName(String womanName) {
        this.womanName = womanName;
    }

    public Bin (String number, String manName, String womanName){
        setNumber(number);
        setManName(manName);
        setWomanName(womanName);
    }

    @Override
    public String toString() {
        return "Bin{" + "number = " + getNumber() + ", manName = " + getManName() + ", womanName = " + getWomanName() + "}\n";
    }
}
