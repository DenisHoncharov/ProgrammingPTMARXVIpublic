package ua.org.oa.honcharov.HomeWork_2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by den4ik on 20.04.2016.
 */
public class StringUtils {

    public static String reversSentence(String sentence) {
        StringBuilder sb = new StringBuilder();
        for (int i = sentence.length() - 1; i >= 0; i--) {      //этот цыкл проходит по всей длине строки от конца
            sb.append(sentence.charAt(i));                      // и ложит в StringBuilder всю строку с конца по букве
        }
        return sb.toString();
    }

    public static Boolean polindrom(String sentence) {
        if (deleteSpace(sentence).equalsIgnoreCase(reversSentence(deleteSpace(sentence)))) {    // ложит предложение в метод deleteSpace и сравнивает одно предложение без слов с другим но уже перевернутым
            return true;                                                                        // с другим но уже перевернутым с помощью метода reversSentence
        }
        return false;
    }

    // this method for true work method polindrom ↓
    public static String deleteSpace(String deleteSpaceSentence) {  // удаляет из предложения все пробелы
        String noSpace = deleteSpaceSentence.replaceAll(" ", "");   // replaceAll меняет все пробелы на ""(ничего)
        return noSpace;
    }

    public static String calkStringLenght(String sentence) {
        String complitSentence;
        if (sentence.length() > 10) {
            complitSentence = sentence.substring(0, 6);      // если больше 10, режет предложение до 6
            return complitSentence;
        }
        return complitSentence = sentence.concat("oooooooooooo").substring(0, 12);  // если меньше 10, добовляет 12 буков "О" и режет предложение до 12
    }

    public static String swapLastWordInString(String text) {
        StringBuilder sb = new StringBuilder();
        String s;
        String wordsArray[] = text.split(" ");
        for (int j = 0; j < wordsArray[wordsArray.length - 1].length(); j++) {      // идет по плине последнего слова в массиве
            if (wordsArray[wordsArray.length - 1].charAt(wordsArray[wordsArray.length - 1].length() - 1) == '.') {  // проверяет на совпадение последнего символа к точке "."
                wordsArray[wordsArray.length - 1] = wordsArray[wordsArray.length - 1].substring(0, wordsArray[wordsArray.length - 1].length() - 1); // если совпадение нашлось, удаляет точку
            }
        }
        s = wordsArray[0];                                                                                              //пузырьковым методом: ложим первое слово массива в переменную
        wordsArray[0] = wordsArray[wordsArray.length - 1];                                                                //в первую ячейку массива ложим слово из последней ячейки
        wordsArray[wordsArray.length - 1] = s;                                                                            //в последнюю ложим переменную в последнюю ячейку
        for (int i = 0; i < wordsArray.length; i++) {                                                                     // переводим обычный StringArray в StringBuilder и добовляем пробелы
            sb.append(wordsArray[i] + " ");
        }
        return sb.toString();
    }

    public static String swapLastWordInSentence(String text) {
        StringBuilder sb = new StringBuilder();
        String sentenceArray[] = text.split("\\. ");                          //режем текст на ". "(точка пробел) подходит только такая комбанация
        for (int i = 0; i < sentenceArray.length; i++) {                       //в каждом предложении меняем первое и последнее слово с помощью swapLastWordInString
            sb.append(swapLastWordInString(sentenceArray[i]) + ".");          //добавляем в StringBuilder с точкой
        }
        return sb.toString();
    }

    public static Boolean enterTheLatter(String sentence) {
        int calcLatter = 0;                                                                                             //счетчик который считает количество букв "abc"
        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.charAt(i) == 'a' || sentence.charAt(i) == 'b' || sentence.charAt(i) == 'c') {
                calcLatter++;
            }
        }
        if (calcLatter == sentence.length()) {                                                                           // сравнение количества букв "abc" с длиной строки
            return true;
        }
        return false;
    }

    public static Boolean foundData(String sentence) {
        Pattern pattern = Pattern.compile("(\\d{2})[.](\\d{2})[.](\\d{4})");
        Matcher matcher = pattern.matcher(sentence);
        if (matcher.matches() == false) {                                          //проверяем в правильном ли формате задана дата
            return false;
        } else {
            int dd = Integer.parseInt(matcher.group(2));                           //парсим в инт
            int mm = Integer.parseInt(matcher.group(1));                           //парсим в инт
            int yyyy = Integer.parseInt(matcher.group(3));                         //парсим в инт
            if (dd <= 31 && mm <= 12) {
                return true;
            }
            return false;
        }
    }

    public static Boolean foundMail (String sentence){
        Pattern pattern = Pattern.compile("\\w+@\\w+[.]\\w{2,3}");
        Matcher matcher = pattern.matcher(sentence);
        return matcher.matches();
    }

    public static String foundPhoneNumber (String sentence){
        Pattern pattern = Pattern.compile("[+]\\d[(]\\d{3}[)]\\d{3}[-]\\d{2}[-]\\d{2}");
        Matcher matcher = pattern.matcher(sentence);
        StringBuilder phoneNumber = new StringBuilder();
        while (matcher.find()){
            phoneNumber.append(matcher.group()+"\n");
        }
        return "Phone Number: \n" + phoneNumber.toString();
    }

    public static String markdownParcer (String markdownText){
        Pattern pattern = Pattern.compile("");
        Matcher matcher = pattern.matcher(markdownText);
        return null;
    }
}
