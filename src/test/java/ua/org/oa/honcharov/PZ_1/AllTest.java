package ua.org.oa.honcharov.PZ_1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ArrayProdTest.class, ArraySumTest.class})

public class AllTest {

}
