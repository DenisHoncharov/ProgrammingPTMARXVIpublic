package ua.org.oa.honcharov.PZ_1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by student on 04.04.2016.
 */
public class ArrayProdTest {

    static int[] testArrray;

    @BeforeClass
    public static void initArray(){
        testArrray = new int[] {1,2,3,4};
    }

    @Test
    public void testMultiplyArray() {
        int actual = ArrayProd.multiplyArray(testArrray);
        int expected = 24;
        Assert.assertEquals("Test multiply array", expected, actual, 0.01);
    }
}