package ua.org.oa.honcharov.PZ_1;

import org.junit.Assert;
import org.junit.*;

import static org.junit.Assert.*;

/**
 * Created by student on 04.04.2016.
 */
public class ArraySumTest {

    static int[] testArrray;

    @BeforeClass
    public static void initArray(){
        testArrray = new int[]{0,1,2,3,4};
    }


    @Test
    public void testSumStatic() {
        int actual = ArraySum.sum(testArrray);
        int expected = 10;

        Assert.assertEquals("Test sum array", expected, actual, 0.01);
    }


    @Test
    public void testSum() {
        ArraySum mySum = new ArraySum(testArrray);
        int actual = mySum.sum();
        int expected = 10;

        Assert.assertEquals("Test sum array", expected, actual, 0.01);
    }

    @Test(expected = java.lang.NullPointerException.class)
    public void testSumStaticNull() {
        ArraySum.sum(null);
    }



}